# Por una correcta higiene digital

Apuntes para un breve taller de introducción a la autodefensa digital

19 Noviembre 2018

basado en los textos de:
* **Cripto libretto** - Unit hacklab - unit@paranoici.org
* **Zen y el arte de que la tecnología trabaje para ti** - Tactical Tech - https://gendersec.tacticaltech.org/wiki/index.php/Complete_manual/es

## Introducción

Usa software libre y de código abierto

https://www.gnu.org/philosophy/free-sw.es.html

## Una buena contraseña

No usar la misma contraseña para diversos servicios.
Usa una passphrase, es decir, una frase de acceso que contiene espacios, facil de recordar, pero difícil de adivinar por un computador. Por ejemplo, una frase que contenga mayúsculas, números, espacios y signos de puntuación, pero fácil de recordar:

Los silenciosos años de las expropiaciones venideras.

## El gestor de contraseñas

Muchas veces las contraseñas a recordar son demasiadas, KeePassX es un software multiplataforma para la gestión de password, un lugar seguro donde escribirlas mientras no olvides la contraseña principal.

https://www.keepassx.org

## Cegar la cámara del portatil

Pega un pedazo de cinta adhesiva negra de electricista sobre la lente de la cámara del portatil. 

Recuerda: el hecho de que no se encienda el punto de luz lateral no significa que esté apagada.

## Navegación consciente

### Uso Mozilla Firefox

La gestión de los contenedores y los perfiles nos permite crear ambientes aislados.

#### Contenedores

Crea espacios independientes para la visión de contenidos en el mismo navegador. Cuida de que las pestañas con diversos contenidos no compartan contenido que pueda comprometer tu seguridad

https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers

#### Perfiles

Son las diversas sesiones del navegador, para compartir también las extensiones. Para abrir los diversos perfiles puedes escribir en la ventana del navegador:

*about:profiles*

#### Extensiones de interés

Para descubrir e instalar las extensiones debes ir al menú general de Firefox y buscar Add-ons

* **Https-everywhere:** Obliga al uso de HTTPS frente a HTTP
* **Ghostery:** Bloquea los trackers (analíticas, ...) en la navegación
* **No Script:** Bloquea scripts no autorizados en la navegación
* **Ublock Origin:** Bloqueo de publicidad (incluso en los vídeos...)

## Navegación anónima con TOR

**TOR**, The Onion Router es un protocolo para la anonimación del tráfico web. Descarga, instala y usa el programa Tor Browser Bundle para navegar en red anónimamente.

¡Atención! No creas que todo funciona por arte de magia, estudia, prueba y verifica todos tus pasos en el uso de la red TOR.

https://www.torproject.org/download/download.html.es

## Tails

**TAILS** es un sistema operativo amnésico, se incia a través de un stick USB y cuando se desconecta todo lo sucecido no queda eliminado. En otras palabras, utiliza TOR para la navegación a través de internet y permite usar el ordenador sin necesidad de instalar nada. Útil para el uso en viages.

https://tails.boum.org

## Verificación de la integridad del software descargado

Algunos software ofrecen verificar su integridad a través de un checksum (suma de control), o sea, una cadena alfanumérica única que resulta al aplicar un algoritmo. La comprobación se basa en la comparación del resultado al aplicar la suma en el paquete descargado y la versión original en la web de lxs desarrolladorxs.

Un ejemplo del uso:

```
wget https://www.veracrypt.fr/en/Downloads.html
sha256sum VeraCrypt_1.23_Bundle.7z
48cb8e15a0383b59cadf7b73067da43e253451aca6c2d32b881966d0c854fa2c
```

Comprobamos que el valor es el mismo que en la web https://launchpad.net/veracrypt/trunk/1.23/+download/VeraCrypt_1.23_Bundle.7z.checksums

Otros software también nos ofrecen la posibilidad de comprobar a través de GnuPG la firma de lxs desarrolladorxs. 

```
gpg --verify VeraCrypt_1.23_Bundle.7z.sig VeraCrypt_1.23_Bundle.7z
```

## Usa un sistema operativo libre

Existen diversas distribuciones de Gnu/Linux para ser instaladas de maneras muy sencillas en todo tipo de ordenadores.

Aquí una breve lista por orden de facilidad de uso:

* **Ubuntu** - https://www.ubuntu.com/desktop/
* **Linux Mint** - https://linuxmint.com
* **Debian** - https://www.debian.org

## Comunicación segura desde el smartphone

**Premisa:** Consideramos que *los smartphones son inseguros* por definición. Pocos dispositivos pueden ser liberados de sus sistemas operativos propietarios y su código en gran parte no es abierto.

Listado de aplicaciones de comunicación instantanea de código abierto:

* **Signal** - App de comunicación privada con encriptación punto a punto. Requiere un número de teléfono. Multiplataforma. No requiere de los servicios de Google para funcionar - https://signal.org
* **Conversations** - App de comunicación basada en servicios XMPP. No requiere número de telefóno. Multiplataforma. Requiere una cuenta Jabber para ser utilizada - https://conversations.im
* **DeltaChat** - App de comunicación basada en tu cuenta de correo electrónico. Encriptación punto a punto. No requiere número de teléfono - https://delta.chat
* **Riot.im** - App de comunicación basada en el protocolo Matrix. Encriptación OLM. Requiere cuenta en un servidor federado en Matrix. - https://about.riot.im/

## VeraCrypt

**VeraCrypt** es un software que permite proteger con una passphrase un dispositivo de almacenamiento USB. Útil para transportar documentos en viages sin preocuparse de perderlo. El dispositivo debe ser formateado (borrado de su contenido) antes de ser utilizado por VeraCrypt.

https://www.veracrypt.fr/en/Home.html

## PGP, la criptografía de claves asimétricas

PGP Pretty Good Privacy, es un software de criptografía asimétrica. GnuPG es su equivalente libre.

Se usa para cifrar y firmar documentos (mensajes, archivos, ...)

Quien usa GnuPG crea dos llaves, una privada que sirve para descifrar o firmar y que solo debe tenerla en posesión el usuario que la ha generado, y una clave pública que sirve para encriptar y que será entregada a todos aquellos con quien se quiera contactar.

Para generar una llave:
```
gpg --full-generate-key
```

Esto nos generará una clave con un identificador <key-id>

Crear un certificado de revocación (útil para destruir la clave en caso de pérdida)
```
gpg -o clave-revocacion.asc --gen-revoke <key-id>
```

Importar la llave pública de otra persona
```
gpg --import clave-publica-amiga.asc
```

Verificar un fingerprint (comprobar la validez de la clave)
```
gpg --fingerprint <key-id>
```

Cifrar un documento para Snowden
```
gpg -a -o mensaje-encriptado.asc --encrypt --recipient snowden-Key-ID mensaje-en-claro.txt
```

Descifrar un documento
```
gpg -o mensaje-en-claro.txt mensaje-encriptado.asc
```

Descargar la clave pública desde un servidor de clave
```
gpg --keyserver gpg.mit.edu --search-keys <key-id>
```

Verificar una firma de un documento
```
gpg --verify documento.sig documento
```

## Más recursos

### Protege tu privacidad en la red

* **Prism Break** Un listado de programas para escapar de los sistema de vigilancia global - https://prism-break.org/es/
* **Me and my shadow** Proyecto del colectivo Tactical Tech - https://myshadow.org/

### Servidores libres autogestionados 
Diversos colectivos en el estado y otras partes del mundo ofrecen servicios seguros sin vulnerar tu privacidad. 

**Piperrak** - Colectivo autogestionado de servicios para colectivos locales en Euskal Herria - https://we.piperrak.cc
**Sindominio** - Una apuesta por la telemática antagonista y por la inteligencia colectiva - https://sindominio.net
**Autistici/Inventati** - A/I ofrece una amplia gama de servicios gratuitos, libres y respetuosos de la privacidad - https://www.autistici.org
**Riseup**: organización autónoma con sede en Seattle y con personas comprometidas a lo largo de todo el mundo - https://riseup.net/es/about-us

**Nunca te fies de quien te da soluciones mágicas**

**El ordenador no tiene un cerebro, usa el tuyo**
